package com.example.oci.JSpice.src.knowm.valuehandling;

import android.os.Build;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import org.hibernate.validator.spi.valuehandling.ValidatedValueUnwrapper;

import java.lang.reflect.Type;
import java.util.OptionalInt;

/**
 * A {@link ValidatedValueUnwrapper} for {@link OptionalInt}.
 *
 * <p>Extracts the value contained by the {@link OptionalInt} for validation, or produces {@code
 * null}.
 */
public class OptionalIntValidatedValueUnwrapper extends ValidatedValueUnwrapper<OptionalInt> {
  @RequiresApi(api = Build.VERSION_CODES.N)
  @Override
  @Nullable
  public Object handleValidatedValue(final OptionalInt optional) {
    return optional.isPresent() ? optional.getAsInt() : null;
  }

  @Override
  public Type getValidatedValueType(final Type type) {
    return Integer.class;
  }
}
