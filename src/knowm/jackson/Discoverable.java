package com.example.oci.JSpice.src.knowm.jackson;

/**
 * A tag interface which allows Dropwizard to load Jackson subtypes at runtime, which enables
 * polymorphic configurations.
 */
public interface Discoverable {}
