package com.example.oci.JSpice.src.knowm.valuehandling;

import android.os.Build;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import org.hibernate.validator.spi.valuehandling.ValidatedValueUnwrapper;

import java.lang.reflect.Type;
import java.util.OptionalDouble;

/**
 * A {@link ValidatedValueUnwrapper} for {@link OptionalDouble}.
 *
 * <p>Extracts the value contained by the {@link OptionalDouble} for validation, or produces {@code
 * null}.
 */
public class OptionalDoubleValidatedValueUnwrapper extends ValidatedValueUnwrapper<OptionalDouble> {
  @RequiresApi(api = Build.VERSION_CODES.N)
  @Override
  @Nullable
  public Object handleValidatedValue(final OptionalDouble optional) {
    return optional.isPresent() ? optional.getAsDouble() : null;
  }

  @Override
  public Type getValidatedValueType(final Type type) {
    return Double.class;
  }
}
