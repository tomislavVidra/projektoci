package com.example.oci;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.io.IOException;

public class ShowImageActivity extends AppCompatActivity {
    Button primijeni;
    Button gotovo;

    SeekBar thresholdBar;
    TextView thresholdText;
    SeekBar blurBar;
    TextView blurText;
    SeekBar dilateBar;
    TextView dilateText;
    SeekBar elipseBar;
    TextView elipseText;

    ImageView mImageView;
    Uri imageUri;
    Bitmap imageBitmap;
    Bitmap openCVresultImage;
    boolean primijenio = false;

    double threshold = 100;
    double blur = 2;
    double dilate = 2;
    double elipse = 6;
    String PROVIDER;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_image);

        //slider za threshold
        thresholdText = (TextView)findViewById(R.id.textView3);
        thresholdBar = (SeekBar) findViewById(R.id.seekBar);
        thresholdBar.setMax(255);
        thresholdBar.setProgress(100);
        thresholdText.setText("Threshold = " + 100);
        thresholdBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                thresholdText.setText("Threshold = " + progress);
                threshold = progress;
            }
            public void onStartTrackingTouch(SeekBar seekBar) { }
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        //slider za blur
        blurText = (TextView) findViewById(R.id.textView4);
        blurBar = (SeekBar) findViewById(R.id.seekBar3);
        blurBar.setMax(30);
        blurBar.setProgress(2);
        blurText.setText("Blur = " + 2);
        blurBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                blurText.setText("Blur = " + progress);
                blur = progress;
            }
            public void onStartTrackingTouch(SeekBar seekBar) { }
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        //slider za dilataciju
        dilateText = (TextView) findViewById(R.id.textView5);
        dilateBar = (SeekBar) findViewById(R.id.seekBar2);
        dilateBar.setMax(30);
        dilateBar.setProgress(2);
        dilateText.setText("Dilate = " + 2);
        dilateBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                dilateText.setText("Dilate = " + progress);
                dilate = progress;
            }
            public void onStartTrackingTouch(SeekBar seekBar) { }
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        //slider za nepoznatu cetvrtu stvar
        elipseText = (TextView) findViewById(R.id.textView6);
        elipseBar = (SeekBar) findViewById(R.id.seekBar4);
        elipseBar.setMax(30);
        elipseBar.setProgress(6);
        elipseText.setText("elipse = " + 6);
        elipseBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                elipseText.setText("elipse = " + progress);
                elipse = progress;
            }
            public void onStartTrackingTouch(SeekBar seekBar) { }
            public void onStopTrackingTouch(SeekBar seekBar) { }
        });

        //tipka koja primjenjuje slidere
        Button primijeni = (Button) findViewById(R.id.button2);
        primijeni.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                primijenio = true;
                new Thread(new Runnable() {
                    public void run() { //novi thread da glavni UI thread moze raditi normalno, spora performansa
                        openCVresultImage = OpenCV_ProcessingImage.doOpenCVthing(imageBitmap, threshold, blur, dilate, elipse, PROVIDER);
                        mImageView.post(new Runnable() {
                            public void run() {
                                mImageView.setImageBitmap(openCVresultImage);
                            }
                        });
                    }
                }).start();

            }
        });

        //tipka za nastavak na sljedeci ekran
        Button gotovo = (Button) findViewById(R.id.button_apply);
        gotovo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!primijenio){
                    new Thread(new Runnable() {
                        public void run() { //novi thread da glavni UI thread moze raditi normalno, spora performansa
                            openCVresultImage = OpenCV_ProcessingImage.doOpenCVthing(imageBitmap, threshold, blur, dilate, elipse, PROVIDER);
                            mImageView.post(new Runnable() {
                                public void run() {
                                    mImageView.setImageBitmap(openCVresultImage);
                                }
                            });
                        }
                    }).start();
                }
                Intent intentNextActivity = new Intent(getBaseContext(), SolveImage_Activity.class);
                intentNextActivity.putExtra("uriImg", imageUri.toString());
                startActivity(intentNextActivity);
            }
        });

        //pocetno postavljanje slike
        mImageView = findViewById(R.id.imageShow);
        imageUri = Uri.parse(getIntent().getStringExtra("uriImg"));
        try {
            imageBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
        } catch (IOException e) {
            e.printStackTrace();
        }
        //openCVresultImage= OpenCV_ProcessingImage.doOpenCVthing(imageBitmap, 5);
        mImageView.setImageBitmap(imageBitmap);

    }

}
