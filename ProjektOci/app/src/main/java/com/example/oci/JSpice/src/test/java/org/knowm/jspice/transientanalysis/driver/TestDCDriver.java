/**
 * jspice is distributed under the GNU General Public License version 3
 * and is also available under alternative licenses negotiated directly
 * with Knowm, Inc.
 *
 * Copyright (c) 2016-2017 Knowm Inc. www.knowm.org
 *
 * Knowm, Inc. holds copyright
 * and/or sufficient licenses to all components of the jspice
 * package, and therefore can grant, at its sole discretion, the ability
 * for companies, individuals, or organizations to create proprietary or
 * open source (even if not GPL) modules which may be dynamically linked at
 * runtime with the portions of jspice which fall under our
 * copyright/license umbrella, or are distributed under more flexible
 * licenses than GPL.
 *
 * The 'Knowm' name and logos are trademarks owned by Knowm, Inc.
 *
 * If you have any questions regarding our licensing policy, please
 * contact us at `contact@knowm.org`.
 */
package com.example.oci.JSpice.src.test.java.org.knowm.jspice.transientanalysis.driver;

/*
public class TestDCDriver extends TestDrivers {

  /**
   * @param args
   */
 /* public static void main(String[] args) {

    TestDCDriver testDrivers = new TestDCDriver();
    testDrivers.test();
  }

  @Test
  public void test() {

    Driver driver = new DC("Vdc", 2.5);

    BigDecimal stopTime = new BigDecimal("2");
    BigDecimal timeStep = new BigDecimal(".01");

    List<Number> xData = new ArrayList<>();
    List<Number> yData = new ArrayList<>();

    BigDecimal firstPoint = BigDecimal.ZERO;
    for (BigDecimal t = firstPoint; t.compareTo(stopTime) < 0; t = t.add(timeStep)) {
      if (counter == point2Verify) {
        y = driver.getSignal(t);
      }
      counter++;
      xData.add(t);
      yData.add(driver.getSignal(t));
    }

    // System.out.println(xData);
    // System.out.println(yData);
    // System.out.println(y);

    assertThat(xData.size()).isEqualTo(200);
    assertThat(y).isCloseTo(2.5, within( .01));

    //    plotData("V(in)", xData, yData);
  }
}*/