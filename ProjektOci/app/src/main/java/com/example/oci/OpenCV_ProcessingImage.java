package com.example.oci;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class OpenCV_ProcessingImage {

    public static int numberOfResistors;
    public static List<Resistor> listaOtpornika = new ArrayList<>();

    public static Bitmap doOpenCVthing(Bitmap imageBitmap, double threshold, double blur, double dilate, double elipse, String PROVIDER) {

        if(PROVIDER=="CAMERA");
        if(PROVIDER=="GALLERY");

        Bitmap grayBitmap;


        Mat Rgba = new Mat();
        Mat grayMat = new Mat();

        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inDither = false;
        o.inSampleSize = 4;

        int width = imageBitmap.getWidth();
        int height = imageBitmap.getHeight();

        grayBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);

        Utils.bitmapToMat(imageBitmap, Rgba);
        Imgproc.cvtColor(Rgba, grayMat, Imgproc.COLOR_RGB2GRAY);
        Utils.matToBitmap(grayMat, grayBitmap);


        //dodan dio za Gaussa..
        grayBitmap=grayBitmap.copy(Bitmap.Config.RGB_565,  true);
        Mat src = new Mat ();
        Mat dest = new Mat ();
        Utils.bitmapToMat(grayBitmap, src);


        //size je za promjenu jacine blura
        Imgproc.blur(src, dest, new Size(blur, blur)); //blur je size
        Bitmap processImg = Bitmap.createBitmap(dest.cols(), dest.rows(), Bitmap.Config.RGB_565);
       // Utils.matToBitmap(dest, processImg);

           //Mat src1 = new Mat();
           //Utils.bitmapToMat(processImg, src1);
           //Imgproc.adaptiveThreshold(src1, dest, 125, Imgproc.ADAPTIVE_THRESH_MEAN_C,
                   //Imgproc.THRESH_BINARY, 11, 12);
           //Utils.matToBitmap(grayMat, grayBitmap);
           // mImageView.setImageBitmap(grayBitmap);


        //Dodan treshold, ako se maknu komentari na Gaussu, umjesto greymat stavite dest
        //
        //   Mat src1 = new Mat();
        //   Utils.bitmapToMat(processImg, src1);
        //   Imgproc.threshold(src1, grayMat, 200, 255,Imgproc.THRESH_BINARY_INV);
        //   Utils.matToBitmap(grayMat, grayBitmap);
        //    mImageView.setImageBitmap(grayBitmap);


        //dio za konture
        int w = 255;
        Mat cannyOutput = new Mat();

        //Erode Funkcija
        //
        /*Mat kernelErode = Imgproc.getStructuringElement(
                Imgproc.MORPH_ELLIPSE, new Size(5, 5));
        Imgproc.erode(dest, dest, kernelErode);*/


        //dio za iscrtavanje rubova
        Imgproc.Canny(dest, cannyOutput, threshold, threshold*2);


        //Dilatacija
        Mat kernelDilate = Imgproc.getStructuringElement(
                Imgproc.MORPH_RECT, new Size(dilate, dilate));
        Imgproc.dilate(cannyOutput, cannyOutput, kernelDilate);


        //Pronalazak svih kontura
        List<MatOfPoint> contours = new ArrayList<>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(cannyOutput, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);
        Mat drawing = Mat.zeros(cannyOutput.size(), CvType.CV_8UC4);


        //algoritam za racunanje indeksa najvece konture... rez zapisan u varijablu maxInd
        int maxInd=0;
        int maxW=0;
        for (int i=0; i< contours.size(); i++) {
            MatOfPoint matOfPoint = contours.get(i);
            Rect rect = Imgproc.boundingRect(matOfPoint);
            if((rect.height + rect.width) >= maxW) {
                maxW = rect.height+rect.width;
                maxInd = i;
            }
        }


        Scalar white = new Scalar(w,w,w);
        Scalar red = new Scalar(255, 0, 0);
        Scalar black = new Scalar(0,0,0);

        //Ispis najvece konture
        //
        //Imgproc.drawContours(drawing, contours, maxInd, white, 1, Imgproc.LINE_4, hierarchy, 0, new Point());
        System.out.println("najveci indeks"+maxInd);


        //djeca se dodaju u novu listu Djeca, a izbacuje se glavni vodic--> najveca kontura...
        List<MatOfPoint> Djeca= new ArrayList<>();
        for (int i = 0; i<contours.size();i++) {
            if(i==maxInd) continue;
            else {
                Djeca.add(contours.get(i));
            }
        }


        //u listu squares se spremaju svi otpornici odnosno pravokutnici
        List<MatOfPoint> squares = null;
        for (MatOfPoint c : Djeca) {

            if ((OpenCV_ProcessingImage.isContourSquare(c, elipse))) {

                if (squares == null)
                    squares = new ArrayList<MatOfPoint>();
                squares.add(c);
            }
        }


        //u listu cvorovi se spremaju sve konture koje nisi otpornici(pravokutnici)
        /*List<MatOfPoint> cvorovi = new ArrayList<>();
        for (MatOfPoint m : contours) {
            if(squares.contains(m)) continue;
            else {
                cvorovi.add(m);
            }
        }*/

        System.out.println("pravokutnika imaaa"+ squares.size());


        //metoda za iscrtavanje svih kontura u listi contours..
         Imgproc.drawContours(drawing, contours, -1 , white, -1);


        //ova naredba ispisuje sve detektirane pravokutnike sadrzane u listi squares...
        //za ovakav ispit parametar contourldx mora biti negativan..
        //u slucaju kad je u metodi ispod stavljena black boja tada ce
        //otponici(pravokutnici iz liste squares) biti izbrisani na konacnoj slici --> ovo nije potrebno


        Imgproc.drawContours(drawing, squares, -1, black, 10);

        List<MatOfPoint> cvoroviNew = new ArrayList<>();
        Mat hierarchyNew = new Mat();

        //Potencijaci??
        //
        //Imgproc.findContours(drawing, cvoroviNew, hierarchyNew, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);
        //System.out.println("istog potencijala ima tocaka:"+cvoroviNew.size());


        System.out.println("Pokazuje tu velicinu polja kontura" + contours.size());


        Bitmap process = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);


        //Dio koda za detekciju kuteva/rubova na slici
        /*Mat temp = new Mat();
        Mat secTemp = new Mat();
        Imgproc.cornerHarris(drawing, temp, 2, 3, 0.04);
        Mat tempDstNorm = new Mat();
        Core.normalize(temp, tempDstNorm,0, 255, Core.NORM_MINMAX);
        Core.convertScaleAbs(tempDstNorm, temp);

        Random r = new Random();
        for (int i = 0; i < tempDstNorm.cols(); i++) {
            for (int j = 0; j < tempDstNorm.rows(); j++) {
                double[] value = tempDstNorm.get(j, i);
                if (value[0] > 150)
                    Imgproc.circle(drawing, new Point(i, j), 2, new Scalar(200, 20, 50), 2);
            }
        }*/

        numberOfResistors = squares.size();
        Utils.matToBitmap(drawing, process);

        Mat test = new Mat();
        Utils.bitmapToMat(process, test);


        Imgproc.cvtColor(test, test, Imgproc.COLOR_RGB2GRAY);


        Imgproc.Canny(drawing, test, threshold, threshold*2);
        
        Mat kernelDilateTest = Imgproc.getStructuringElement(
                Imgproc.MORPH_RECT, new Size(2, 2));
        Imgproc.dilate(test, test, kernelDilateTest);

        Mat test1 = Mat.zeros(test.size(), CvType.CV_8UC1);
        Imgproc.findContours(test, cvoroviNew, hierarchyNew, Imgproc.RETR_EXTERNAL, Imgproc.CHAIN_APPROX_SIMPLE);

        Map<Double,ArrayList<Integer>> mindistance = new HashMap<>();
        for(MatOfPoint m : cvoroviNew) {
            Double min = 10000000.10;
            Integer indeks=10;
            MatOfPoint konturamin = new MatOfPoint();
            List<MatOfPoint> lis = new ArrayList<>();
            Map<MatOfPoint, Double> udaljenost = new HashMap<>();
            ArrayList<Integer> susjedi = new ArrayList<>();
            lis.add(m);
            udaljenost = OpenCV_ProcessingImage.MinIntersectionLine(lis, cvoroviNew);

            for(MatOfPoint mp : udaljenost.keySet()) {
                if(udaljenost.get(mp)<min && udaljenost.get(mp)!=0) {
                    min=udaljenost.get(mp);
                    konturamin = mp;

                }
            }
            for(MatOfPoint p : cvoroviNew) {
                if(p.equals(konturamin)) indeks = cvoroviNew.indexOf(p);
            }
            susjedi.add(indeks);
            indeks = cvoroviNew.indexOf(m);
            susjedi.add(indeks);

            mindistance.put(min, susjedi);

        }
        List<MatOfPoint> nova = new ArrayList<>();
        Double min3  =1000000.1;
        for(Double b : mindistance.keySet()) {

           if(b<min3) {
               min3 = b;
           }
        }

        /*List<MatOfPoint> novaaaa = new ArrayList();
        novaaaa.add(cvoroviNew.get(mindistance.get(min3).get(0)));
        novaaaa.add(cvoroviNew.get(mindistance.get(min3).get(1)));
        Mat drawin = Mat.zeros(cannyOutput.size(), CvType.CV_8UC4);
        Bitmap proc = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        Imgproc.drawContours(drawin, novaaaa, -1, white, 1);
        Utils.matToBitmap(drawin, proc);*/

        nova.add(cvoroviNew.get(mindistance.get(min3).get(0)));
        nova.add(cvoroviNew.get(mindistance.get(min3).get(1)));
        for(MatOfPoint b : cvoroviNew) {
            if(cvoroviNew.indexOf(b)!=mindistance.get(min3).get(0) && cvoroviNew.indexOf(b)!=mindistance.get(min3).get(1)) {
                nova.add(b);
            }
        }

        //metoda ispod u test1 sprema konture it liste koja zadana kao drugi parametar, treći parametar u metodi predstavlja koji
        // element liste zelimo prikazati... ako je treći parametar -1 to znaci da zelimo iscrtat sve elemente liste
        Bitmap testBit = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);

        System.out.println("istog potencijala ima tocaka:"+cvoroviNew.size());

        //List<MatOfPoint> testOtpornik = new ArrayList<MatOfPoint>();

        Map<MatOfPoint, ArrayList<MatOfPoint>> rez = new HashMap<>();

        for (MatOfPoint p : squares) {

            List<MatOfPoint> testOtpornik = new ArrayList<MatOfPoint>();
            ArrayList<MatOfPoint> najblizeKonture = new ArrayList<>();
            Map<MatOfPoint, Double> testDistanci = new HashMap<>();

            testOtpornik.add(p);
            //ubacena nova umjesto cvoroviNew
            testDistanci = OpenCV_ProcessingImage.MinIntersectionLine(testOtpornik, nova);


            double min1 = 1000000001, min2 = 1000000000;

            MatOfPoint index = new MatOfPoint();


            for (MatOfPoint contura : testDistanci.keySet()) {

                if (testDistanci.get(contura) < min1) {
                    min1 = testDistanci.get(contura);
                    index = contura;
                }

            }

            najblizeKonture.add(index);

            testDistanci.remove(index);


            for (MatOfPoint contura : testDistanci.keySet()) {

                if (testDistanci.get(contura) < min2) {
                    min2 = testDistanci.get(contura);
                    index = contura;
                }

            }


            testDistanci.clear();
            najblizeKonture.add(index);
            rez.put(p, najblizeKonture);
            testOtpornik.remove(0);

        }



        int index1 = 0, index2 = 0;
        int indexOtpornika = 1;
        int i = 0;

        for (MatOfPoint rezistor : rez.keySet()){


            MatOfPoint c1 = rez.get(rezistor).get(0);
            MatOfPoint c2 = rez.get(rezistor).get(1);
            //stavljena nova umjesto cvoroviNew i maknut  +1 na 346 i 348 liniji
            for (MatOfPoint cvor : nova){
                if (c1.equals(cvor))
                    index1 = nova.indexOf(cvor) ;
                if (c2.equals(cvor))
                    index2 = nova.indexOf(cvor) ;
            }

            List<MatOfPoint> rezzz = new ArrayList<>();
            rezzz.add(rezistor);
            Mat slika= Mat.zeros(cannyOutput.size(), CvType.CV_8UC4);
            Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
            Imgproc.drawContours(slika, rez.get(rezistor), -1, white, 1);
            Imgproc.drawContours(slika, rezzz, -1, red, 1);
            Utils.matToBitmap(slika, bitmap);
            Resistor otpornik = new Resistor(indexOtpornika, index1, index2, bitmap);
            rezzz.remove(0);
            listaOtpornika.add(otpornik);
            indexOtpornika++;
        }




        return process;
    }

    public static boolean isContourSquare(MatOfPoint thisContour, double elipse) {

        Rect ret = null;

        MatOfPoint2f thisContour2f = new MatOfPoint2f();
        MatOfPoint approxContour = new MatOfPoint();
        MatOfPoint2f approxContour2f = new MatOfPoint2f();

        thisContour.convertTo(thisContour2f, CvType.CV_32FC2);

        Imgproc.approxPolyDP(thisContour2f, approxContour2f, elipse, true);

        approxContour2f.convertTo(approxContour, CvType.CV_32S);

        if (approxContour.size().height == 4) {
            ret = Imgproc.boundingRect(approxContour);
        }

        return (ret != null);
    }

    public static Map<MatOfPoint, Double> MinIntersectionLine(List<MatOfPoint> a, List<MatOfPoint> b)
    {
        double MinDist = 10000000;
        double MinDist2 = 10000000;
        Map<MatOfPoint, Double> testDistanci = new HashMap<>();

        for (int i = 0; i < a.size(); i++)
        {
            for (int j = 0; j < b.size(); j++)
            {
                double Dist = Distance_BtwnPoints(b.get(j), a.get(i));
                if (Dist < MinDist)
                {
                    MinDist = Dist;
                }
                testDistanci.put(b.get(j), Dist);
            }
        }

        return testDistanci;
    }

    public static double Distance_BtwnPoints(MatOfPoint p, MatOfPoint q)
    {
        Point[] arrPoints1 = p.toArray();
        Point[] arrPoints = q.toArray();

        double min=1000000;

        for (int i =0; i < arrPoints1.length; ++i) {


            for(int j=0; j< arrPoints.length; ++j) {
                double X_Diff = arrPoints1[i].x - arrPoints[j].x;
                double Y_Diff = arrPoints1[i].y - arrPoints[j].y;
                double distance = Math.sqrt((X_Diff * X_Diff) + (Y_Diff * Y_Diff));
                if(distance < min) {
                    min=distance;
                }
            }
        }
        return min;
    }


    public static int getNumberOfRes(){

        return numberOfResistors;
    }
}

