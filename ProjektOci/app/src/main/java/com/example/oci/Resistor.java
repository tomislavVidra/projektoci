package com.example.oci;

import android.graphics.Bitmap;

import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.imgproc.Imgproc;

public class Resistor {


    public int indexOtpornika;
    public Integer contura1;
    public Integer contura2;

    public double vrijednost;
    public Bitmap slika;

    public Resistor (int indexOtpornika, int contura1, int contura2, Bitmap bitmap){
        this.contura1 = contura1;
        this.contura2 = contura2;
        this.indexOtpornika = indexOtpornika;
        this.slika = bitmap;

    }

    public void postaviVrijednost(double vrijednost){
        this.vrijednost = vrijednost;
    }
}
