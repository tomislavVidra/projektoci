package com.example.oci.JSpice.src.knowm.konfig;

import com.example.oci.JSpice.src.knowm.configuration.ConfigurationException;
import com.example.oci.JSpice.src.knowm.configuration.ConfigurationFactory;
import com.example.oci.JSpice.src.knowm.configuration.JsonConfigurationFactory;
import com.example.oci.JSpice.src.knowm.configuration.YamlConfigurationFactory;
import com.example.oci.JSpice.src.knowm.configuration.provider.ConfigurationSourceProvider;
import com.example.oci.JSpice.src.knowm.configuration.provider.FileConfigurationSourceProvider;
import com.example.oci.JSpice.src.knowm.configuration.provider.ResourceConfigurationSourceProvider;
import com.example.oci.JSpice.src.knowm.configuration.provider.UTF8StringConfigurationSourceProvider;
import com.example.oci.JSpice.src.knowm.jackson.Jackson;
import com.example.oci.JSpice.src.knowm.validation.BaseValidator;

import java.io.IOException;

import javax.validation.Validator;

public class Konfig<T extends Konfigurable> {

  public T buildConfigurationfromYAMLString(Class<T> klass, String configAsYAMLString)
      throws IOException, ConfigurationException {

    Validator validator = BaseValidator.newValidator();

    ConfigurationFactory<T> factory =
        new YamlConfigurationFactory<>(klass, validator, Jackson.newObjectMapper(), "k");

    ConfigurationSourceProvider csp = new UTF8StringConfigurationSourceProvider();

    return factory.build(csp, configAsYAMLString);
  }

  public T buildConfigurationfromJSONString(Class<T> klass, String configAsYAMLString)
      throws IOException, ConfigurationException {

    Validator validator = BaseValidator.newValidator();

    ConfigurationFactory<T> factory =
        new JsonConfigurationFactory<>(klass, validator, Jackson.newObjectMapper(), "k");

    ConfigurationSourceProvider csp = new UTF8StringConfigurationSourceProvider();

    return factory.build(csp, configAsYAMLString);
  }

  public T buildConfigurationfromYAMLFileAsResource(Class<T> klass, String resourceName)
      throws IOException, ConfigurationException {

    Validator validator = BaseValidator.newValidator();

    ConfigurationFactory<T> factory =
        new YamlConfigurationFactory<>(klass, validator, Jackson.newObjectMapper(), "k");

    ConfigurationSourceProvider csp = new ResourceConfigurationSourceProvider();

    return factory.build(csp, resourceName);
  }

  public T buildConfigurationfromJSONFileAsResource(Class<T> klass, String resourceName)
      throws IOException, ConfigurationException {

    Validator validator = BaseValidator.newValidator();

    ConfigurationFactory<T> factory =
        new JsonConfigurationFactory<>(klass, validator, Jackson.newObjectMapper(), "k");

    ConfigurationSourceProvider csp = new ResourceConfigurationSourceProvider();

    return factory.build(csp, resourceName);
  }

  public T buildConfigurationfromYAMLFilePath(Class<T> klass, String resourceName)
      throws IOException, ConfigurationException {

    Validator validator = BaseValidator.newValidator();

    ConfigurationFactory<T> factory =
        new YamlConfigurationFactory<>(klass, validator, Jackson.newObjectMapper(), "k");

    ConfigurationSourceProvider csp = new FileConfigurationSourceProvider();

    return factory.build(csp, resourceName);
  }

  public T buildConfigurationfromJSONFilePath(Class<T> klass, String resourceName)
      throws IOException, ConfigurationException {

    Validator validator = BaseValidator.newValidator();

    ConfigurationFactory<T> factory =
        new JsonConfigurationFactory<>(klass, validator, Jackson.newObjectMapper(), "k");

    ConfigurationSourceProvider csp = new FileConfigurationSourceProvider();

    return factory.build(csp, resourceName);
  }
}
