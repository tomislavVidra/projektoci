package com.example.oci;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatDialogFragment;

public class ImageDialog extends AppCompatDialogFragment {
    private EditText inputCurrent;
    private EditText inputVoltage;
    private EditText inputResistance;
    private ImageView circuit;
    private ImageDialogListener listener;
    int ResNum;
    boolean bool;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        bool = getArguments().getBoolean("a1");
        if(bool) { //true = dijalog za struju/napon, false = dijalog za otpor
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View view = inflater.inflate(R.layout.layout_dialog, null); //layout za struju/napon
            String text = getArguments().getString("a2");
            builder.setView(view)
                    .setTitle(text)
                    .setPositiveButton("OK", null);
            inputCurrent =  (EditText)  view.findViewById(R.id.input_current);
            inputVoltage =  (EditText)  view.findViewById(R.id.input_voltage);
        } else{
            LayoutInflater inflater = getActivity().getLayoutInflater();
            View view = inflater.inflate(R.layout.layout_dialog2, null);
            String text = getArguments().getString("a2");
            ResNum = getArguments().getInt("a3");
            builder.setView(view)
                    .setTitle(text)
                    .setPositiveButton("OK", null);
            inputResistance =  (EditText)  view.findViewById(R.id.input_resistance);
            circuit = (ImageView) view.findViewById(R.id.imageView3);
            byte [] encodeByte= Base64.decode(getArguments().getString("a4"),Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            circuit.setImageBitmap(bitmap);
        }

        AlertDialog dialog = builder.create();
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(bool) {
                            if(!(TextUtils.isEmpty(inputCurrent.getText())) && !(TextUtils.isEmpty(inputVoltage.getText()))) {
                                String current = inputCurrent.getText().toString();
                                String voltage = inputVoltage.getText().toString();
                                listener.applyCV(current, voltage); //spremi vrijednost
                                dialog.dismiss();
                            }
                        } else {
                            if(!(TextUtils.isEmpty(inputResistance.getText()))){
                                String resistance = inputResistance.getText().toString();
                                listener.applyResistance(resistance, ResNum); //spremi vrijednost
                                dialog.dismiss();
                            }
                        }
                    }
                });
            }
        });
        dialog.show();
        return dialog;

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        try {
            listener = (ImageDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() +
                    "must implement ImageDialogListener");
        }
    }

    public interface ImageDialogListener {
        void applyCV(String current, String voltage);
        void applyResistance(String resistance, int ResNum);
    }
}