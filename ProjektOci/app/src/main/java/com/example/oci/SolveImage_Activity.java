package com.example.oci;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.oci.JSpice.src.main.java.org.knowm.jspice.JSpice;
import com.example.oci.JSpice.src.main.java.org.knowm.jspice.netlist.Netlist;
import com.example.oci.JSpice.src.main.java.org.knowm.jspice.netlist.NetlistBuilder;

import java.io.ByteArrayOutputStream;
import java.util.List;

public class SolveImage_Activity extends AppCompatActivity implements ImageDialog.ImageDialogListener {
    ImageView mImageView;
    Uri imageUri;
    int numRes = OpenCV_ProcessingImage.listaOtpornika.size(); //koliko otpornika toliko dijalog okvira nakon struja/izvor
    float current;
    float voltage;
    TextView values;
    List<Resistor> listaOtpornika = OpenCV_ProcessingImage.listaOtpornika;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_solve_image);

        imageUri = Uri.parse(getIntent().getStringExtra("uriImg"));
        mImageView = (ImageView) findViewById(R.id.imageShow3);
        mImageView.setImageURI(imageUri);
        values = (TextView) findViewById(R.id.values);

        openFirstDialog();
    }

    public void openFirstDialog() {  //prvi dijalog za struju i napon izvora
        Bundle bundle=new Bundle();
        bundle.putBoolean("a1", true); //true = dijalog za struju/napon
        bundle.putString("a2", "Enter current and source voltage values");
        ImageDialog CVDialog = new ImageDialog();
        CVDialog.setArguments(bundle);
        CVDialog.show(getSupportFragmentManager(), "CV dialog");
    }

    public void openResistanceDialog(int ResNum) { //dijalog za otpornik
            Bundle bundle = new Bundle();
            bundle.putBoolean("a1", false); //false = dijalog za otpor
            bundle.putString("a2", "Enter R" + ResNum + " value");
            bundle.putInt("a3", ResNum);
            bundle.putString("a4", BitMapToString(listaOtpornika.get(ResNum).slika));

            ImageDialog ResistanceDialog = new ImageDialog();
            ResistanceDialog.setArguments(bundle);
            ResistanceDialog.show(getSupportFragmentManager(), "resistance dialog" + ResNum);
    }

    //spremi vrijednosti dijaloga u varijable, pozovi dijalog za otpornik
    @Override
    public void applyCV(String currentD, String voltageD) {
        current = Float.parseFloat(currentD);
        voltage = Float.parseFloat(voltageD);
        values.append("Current = " + current + "A\n" + "Voltage = " + voltage + "V\n");
        openResistanceDialog(0);
    }
    //spremi vrijednosti otpora u varijablu, pozovi sljedeci dijalog za otpornik ako ima
    @Override
    public void applyResistance(String resistance, int ResNum) {
        float resistanceF = Float.parseFloat(resistance);
        values.append("R" + ResNum + " = " + resistanceF + "Ω\n");
        listaOtpornika.get(ResNum).postaviVrijednost(resistanceF);
        if(ResNum+1 < numRes) {
            openResistanceDialog(ResNum + 1);
        } else{
            test();
            //SolveNetlist();
        }
    }

    private void SolveNetlist() {

    }

    public void test(){
        NetlistBuilder builder = new NetlistBuilder().addNetlistDCVoltage("izvor",voltage, "1", "0");
                //.addNetlistResistor("R0", listaOtpornika.get(0).vrijednost, listaOtpornika.get(0).contura1.toString(), listaOtpornika.get(0).contura2.toString())
                //.addNetlistResistor("R1",listaOtpornika.get(1).vrijednost, listaOtpornika.get(1).contura1.toString(), listaOtpornika.get(1).contura2.toString())
                //.addNetlistResistor("R2", listaOtpornika.get(2).vrijednost, listaOtpornika.get(2).contura1.toString(), listaOtpornika.get(2).contura2.toString())
                //.addNetlistResistor("R3",listaOtpornika.get(3).vrijednost, listaOtpornika.get(3).contura1.toString(),listaOtpornika.get(3).contura2.toString() );
        for(Resistor r : listaOtpornika) {
            builder.addNetlistResistor("R"+ (r.indexOtpornika-1), r.vrijednost, r.contura1.toString(), r.contura2.toString());
        }
        Netlist netlist = builder.build();
        JSpice.simulate(netlist, values);
    }

    public String BitMapToString(Bitmap bitmap){
        ByteArrayOutputStream ByteStream=new  ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100, ByteStream);
        byte [] b=ByteStream.toByteArray();
        String temp= Base64.encodeToString(b, Base64.DEFAULT);
        return temp;
    }
}
