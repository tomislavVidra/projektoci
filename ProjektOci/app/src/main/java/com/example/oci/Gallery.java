/*package com.example.oci;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.ContactsContract;
import android.provider.MediaStore;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Rect;
import org.opencv.imgproc.Imgproc;
import org.opencv.core.Size;

import java.io.IOException;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.CvType;

public class Gallery extends AppCompatActivity {
    ImageView mImageView;
    Button mChooseBtn;
    Uri imageUri;
    Bitmap grayBitmap, imageBitmap;

    private static final int IMAGE_PICK_CODE = 1000;
    private static final int PERMISSION_CODE = 1001;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        mImageView = findViewById(R.id.image_view);
        mChooseBtn = findViewById(R.id.choose_image_btn);

        mChooseBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                    if(checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED){
                        String[] permissions = {Manifest.permission.READ_EXTERNAL_STORAGE};
                        requestPermissions(permissions, PERMISSION_CODE);
                    } else{
                        pickImageFromGallery();
                    }
                } else{
                    pickImageFromGallery();
                }
            }
        });
    }

    private void pickImageFromGallery() {
        Intent intent = new Intent(Intent.ACTION_PICK);
        intent.setType("image/*");
        startActivityForResult(intent, IMAGE_PICK_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch(requestCode){
            case PERMISSION_CODE: {
                if(grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    pickImageFromGallery();
                } else{
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if(resultCode == RESULT_OK && requestCode == IMAGE_PICK_CODE){
            imageUri = data.getData();
            try {
                imageBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
            } catch (IOException e) {
                e.printStackTrace();
            }
            convertToGray();
        }
    }

    public static boolean isContourSquare(MatOfPoint thisContour) {

        Rect ret = null;

        MatOfPoint2f thisContour2f = new MatOfPoint2f();
        MatOfPoint approxContour = new MatOfPoint();
        MatOfPoint2f approxContour2f = new MatOfPoint2f();

        thisContour.convertTo(thisContour2f, CvType.CV_32FC2);

        Imgproc.approxPolyDP(thisContour2f, approxContour2f, 6, true);

        approxContour2f.convertTo(approxContour, CvType.CV_32S);

        if (approxContour.size().height == 4) {
            ret = Imgproc.boundingRect(approxContour);
        }

        return (ret != null);
    }


    //u metodi convertToGray dodan Gaussovo zaglađivanje
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void convertToGray(){
        Size si = new Size(500, 500);
        Mat Rgba = new Mat();
        Mat grayMat = new Mat();

        BitmapFactory.Options o = new BitmapFactory.Options();
        o.inDither = false;
        o.inSampleSize=4;

        int width = imageBitmap.getWidth();
        int height = imageBitmap.getHeight();

        grayBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);

        Utils.bitmapToMat(imageBitmap, Rgba);
        Imgproc.cvtColor(Rgba, grayMat, Imgproc.COLOR_RGB2GRAY);
        Utils.matToBitmap(grayMat, grayBitmap);

        //mImageView.setImageBitmap(grayBitmap);

        //dodan dio za Gaussa..
        grayBitmap=grayBitmap.copy(Bitmap.Config.RGB_565,  true);
        Mat src = new Mat ();
        Mat dest = new Mat ();
        Utils.bitmapToMat(grayBitmap, src);
        //size je za promjenu jacine blura
        Imgproc.blur(src, dest, new Size(2, 2));
        Bitmap processImg = Bitmap.createBitmap(dest.cols(), dest.rows(), Bitmap.Config.RGB_565);
        Utils.matToBitmap(dest, processImg);


     //   mImageView.setImageBitmap(processImg);


        //Dodan treshold, ako se maknu komentari na Gaussu, umjesto greymat stavite dest
     //   Mat src1 = new Mat();
     //   Utils.bitmapToMat(processImg, src1);
     //   Imgproc.threshold(src1, grayMat, 200, 255,Imgproc.THRESH_BINARY_INV);
     //   Utils.matToBitmap(grayMat, grayBitmap);
    //    mImageView.setImageBitmap(grayBitmap);

        //dio za konture
        int threshold = 100;
        int w = 255;
        Random rng = new Random(12345);

        Mat cannyOutput = new Mat();

        /*Mat kernelErode = Imgproc.getStructuringElement(
                Imgproc.MORPH_ELLIPSE, new Size(5, 5));
        Imgproc.erode(dest, dest, kernelErode);

        //dio za iscrtavanje rubova
        Imgproc.Canny(dest, cannyOutput, threshold, threshold*2);


        Mat kernelDilate = Imgproc.getStructuringElement(
                Imgproc.MORPH_RECT, new Size(2, 2));
        Imgproc.dilate(cannyOutput, cannyOutput, kernelDilate);



        List<MatOfPoint> contours = new ArrayList<>();
        Mat hierarchy = new Mat();
        Imgproc.findContours(cannyOutput, contours, hierarchy, Imgproc.RETR_TREE, Imgproc.CHAIN_APPROX_SIMPLE);
        Mat drawing = Mat.zeros(cannyOutput.size(), CvType.CV_8UC4);
        MatOfPoint minCont = contours.get(1);
        MatOfPoint maxCont = contours.get(1);
        int j = 0;
        for (MatOfPoint contur : contours) {
            //   Scalar color = new Scalar(rng.nextInt(1), rng.nextInt(1), rng.nextInt(1));
            if (Math.abs(Imgproc.contourArea(contur)) < Math.abs(Imgproc.contourArea(minCont)))
                minCont = contur;
            if (Math.abs(Imgproc.contourArea(contur)) > Math.abs(Imgproc.contourArea(maxCont))) {
                System.out.println("Index najvece conture " + j);
                maxCont = contur;
            }
            j++;
            //Scalar color = new Scalar(w,w,w);

            //Imgproc.drawContours(drawing, contours, i, color, 1, Imgproc.LINE_8, hierarchy, 0, new Point());
        }

        //algoritam za racunanje indeksa najvece konture... rez zapisan u varijablu maxInd
        int maxInd=0;
        int maxW=0;
        for (int i=0; i< contours.size(); i++) {
            MatOfPoint matOfPoint = contours.get(i);
            Rect rect = Imgproc.boundingRect(matOfPoint);
            if((rect.height + rect.width) >= maxW) {
                maxW = rect.height+rect.width;
                maxInd = i;
            }
        }



        /*Mat kernelDilate = Imgproc.getStructuringElement(
                Imgproc.MORPH_RECT, new Size(3, 3));
        Imgproc.dilate(cannyOutput, cannyOutput, kernelDilate);

        Scalar color1 = new Scalar(w,w,w);
        //Scalar color1 = new Scalar(rng.nextInt(256), rng.nextInt(256), rng.nextInt(256));
        Scalar color = new Scalar(255, 0, 0);
        //Ispis najvece konture
       // Imgproc.drawContours(drawing, contours, maxInd, color1, 1, Imgproc.LINE_4, hierarchy, 0, new Point());
        System.out.println("najveci indeks"+maxInd);

        //djeca se dodaju u novu listu Djeca, a izbacuje se glavni vodic--> najveca kontura...
        List<MatOfPoint> Djeca= new ArrayList<>();
        for (int i = 0; i<contours.size();i++) {
            if(i==maxInd) continue;
            else {
                Djeca.add(contours.get(i));
            }
        }

        //u listu squares se spremaju svi otpornici odnosno pravokutnici
        List<MatOfPoint> squares = null;

        for (MatOfPoint c : Djeca) {

            if ((Gallery.isContourSquare(c))) {

                if (squares == null)
                    squares = new ArrayList<MatOfPoint>();
                squares.add(c);
            }
        }

        //u listu cvorovi se spremaju sve konture koje nisi otpornici(pravokutnici)
        List<MatOfPoint> cvorovi = new ArrayList<>();
        for (MatOfPoint m : contours) {
            if(squares.contains(m)) continue;
            else {
                cvorovi.add(m);
            }
        }

        System.out.println("pravokutnika imaaa"+ squares.size());

        //metoda za iscrtavanje svih kontura u listi contours..
       // Imgproc.drawContours(drawing, contours, -1 , color1, 1);

        Scalar crna = new Scalar(0,0,0);
        //ova naredba ispisuje sve detektirane pravokutnike sadrzane u listi squares... za ovakav ispit parametar contourldx mora biti negativan..
        //u slucaju kad je u metodi ispod stavljena crna boja tada ce otponici(pravokutnici iz liste squares) biti izbrisani na konacnoj slici --> ovo nije potrebno
        //Imgproc.drawContours(drawing, squares, -1 , crna, 1);

        Imgproc.drawContours(drawing, squares, -1, color1, 1);

        //Ispis svih konntrua i popuna njihova
        /*for(int i = 0; i < contours.size(); ++i){

            //System.out.println("Ovdje je velicina i-te konture" + i + " --> " + Imgproc.contourArea(contours.get(i)));
            System.out.println("Najmanja kontura " + Imgproc.contourArea(minCont));
            System.out.println("Najveci kontura " + Imgproc.contourArea(maxCont));
            //Scalar color1 = new Scalar(rng.nextInt(256), rng.nextInt(256), rng.nextInt(256));
            if(Math.abs(Imgproc.contourArea(contours.get(i))) > 600)
                Imgproc.drawContours(drawing, contours, i, color, 1, Imgproc.LINE_4 , hierarchy, 0, new Point());
            else
                Imgproc.drawContours(drawing, contours, i, color1, -1, Imgproc.LINE_4, hierarchy, 0, new Point());
        }*/

        /*Mat kernelDilate1 = Imgproc.getStructuringElement(
                Imgproc.MORPH_RECT, new Size(3, 3));
        Imgproc.dilate(drawing, drawing, kernelDilate1);
        System.out.println("Pokazuje tu velicinu polja kontura" + contours.size());
        //getSquerConturs



        Bitmap process = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);

        //Utils.matToBitmap(drawing, process);
        //mImageView.setImageBitmap(process);

      //  Mat kernelDilate = Imgproc.getStructuringElement(
      //          Imgproc.MORPH_RECT, new Size(2, 2));
        //Imgproc.dilate(drawing, drawing, kernelDilate);

        //Dio koda za detekciju kuteva/rubova na slici
      /*  Mat temp = new Mat();
        Mat secTemp = new Mat();
        Imgproc.cornerHarris(drawing, temp, 2, 3, 0.04);
        Mat tempDstNorm = new Mat();
        Core.normalize(temp, tempDstNorm,0, 255, Core.NORM_MINMAX);
        Core.convertScaleAbs(tempDstNorm, temp);

        Random r = new Random();
        for (int i = 0; i < tempDstNorm.cols(); i++) {
            for (int j = 0; j < tempDstNorm.rows(); j++) {
                double[] value = tempDstNorm.get(j, i);
                if (value[0] > 150)
                    Imgproc.circle(drawing, new Point(i, j), 2, new Scalar(200, 20, 50), 2);
            }
        }*/
/*
        Utils.matToBitmap(drawing, process);
        mImageView.setImageBitmap(process);
    }
}
*/