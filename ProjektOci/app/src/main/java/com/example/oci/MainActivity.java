package com.example.oci;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.github.dhaval2404.imagepicker.ImagePicker;
import com.github.dhaval2404.imagepicker.constant.ImageProvider;

import org.opencv.android.OpenCVLoader;


public class MainActivity extends AppCompatActivity {

    private Bitmap photo = null;
    Uri uriImg;
    private static final int READ_EXTERNAL_STORAGE_PERMISSION_CODE = 1000;
    private static final int CAMERA_PERMISSION_CODE = 1001;
    private static String PROVIDER;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (OpenCVLoader.initDebug())
            Toast.makeText(getApplicationContext(), "OpenCV loaded", Toast.LENGTH_SHORT).show();
        else
            Toast.makeText(getApplicationContext(), "OpenCV failed to load", Toast.LENGTH_SHORT).show();

        Button cameraButton = (Button) findViewById(R.id.cameraButton);
        cameraButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PROVIDER="CAMERA";
                Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                ImagePicker.Companion.with(MainActivity.this)
                        .provider(ImageProvider.CAMERA)
                        //.provider(ImageProvider.GALLERY)
                        //without .provider(..) -for camera and gallery

                        //.crop(1f, 1f) //omogucava cropanje po x:y formatu (1:1, 16:9, ...)
                        .compress(1024)//maksimalna velicina slike u KB
                        .maxResultSize(1080, 1080)
                        .start();
            }
        });

        Button galleryButton = (Button) findViewById(R.id.galleryButton);
        galleryButton .setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PROVIDER="GALLERY";
                Intent getIntent = new Intent(Intent.ACTION_GET_CONTENT);
                ImagePicker.Companion.with(MainActivity.this)
                        //.provider(ImageProvider.CAMERA)
                        .provider(ImageProvider.GALLERY)
                        //without .provider(..) -for camera and gallery

                        //.crop(1f, 1f) //omogucava cropanje po x:y formatu (1:1, 16:9, ...)
                        .compress(1024)//maksimalna velicina slike u KB
                        .maxResultSize(1080, 1080)
                        .start();
            }
        });

    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ImagePicker.REQUEST_CODE) {
            if (resultCode == MainActivity.RESULT_OK) {

                //Image Uri will not be null for RESULT_OK
                if (data == null) {
                    Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show();
                    return;
                }

                uriImg = data.getData();

                //You can get File object from intent
                //File file = ImagePicker.Companion.getFile(data);

                //You can also get File Path from intent
                //String filePath = ImagePicker.Companion.getFilePath(data);


                //starting ShowImageActivity activity with uri of Image ("uriImg") passed in Intent
                Intent intentNextActivity = new Intent(getBaseContext(), ShowImageActivity.class);
                intentNextActivity.putExtra("uriImg", uriImg.toString());
                intentNextActivity.putExtra("PROVIDER", PROVIDER);
                startActivity(intentNextActivity);

            } else if (resultCode == ImagePicker.RESULT_ERROR) {
                Toast.makeText(this, ImagePicker.Companion.getError(data), Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(this, "Ne bi trebao dobiti ovu gresku", Toast.LENGTH_SHORT).show();
        }
    }
}
